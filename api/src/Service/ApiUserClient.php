<?php

namespace App\Service;

use App\Exception\ApiClientException;
use App\Service\Response\Order;
use App\Service\Response\SearchOrders;
use App\Service\Response\UsersResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiUserClient
{

    private HttpClientInterface $httpClient;
    private SerializerInterface $serializer;

    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serializer)
    {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
    }

    /**
     * Get User from Api
     *
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws ApiClientException
     */
    public function getUsers(int $quantity): UsersResponse
    {
        $response = $this->httpClient->request(
            "GET",
            sprintf("https://fakerapi.it/api/v1/users?_quantity=%s", $quantity)
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new ApiClientException($response->getContent(false));
        }

        return $this->serializer->deserialize($response->getContent(), UsersResponse::class, 'json');
    }



}