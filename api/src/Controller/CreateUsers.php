<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\User;
use App\Exception\ApiClientException;
use App\Factory\UserFromApiFactory;
use App\Service\ApiUserClient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

#[Route('/users/{quantity}', name: 'create_users_from_api', requirements: [
    'quantity' => '\d+'
], methods: 'POST')]
class CreateUsers extends AbstractController
{

    private ApiUserClient $apiUserClient;
    private SerializerInterface $sezializer;
    private EntityManagerInterface $entityManager;

    public function __construct(ApiUserClient $apiUserClient, SerializerInterface $sezializer, EntityManagerInterface $entityManager)
    {
        $this->apiUserClient = $apiUserClient;
        $this->sezializer = $sezializer;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws ApiClientException
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function __invoke(int $quantity): JsonResponse
    {
        try {

            $usersResponse = $this->apiUserClient->getUsers($quantity);

            if (!count($usersResponse->getData())) {
                throw new \Exception('No User found.');
            }

            foreach ($usersResponse->getData() as $user) {
                if ($this->entityManager->getRepository(User::class)->findOneBy(['uuid' => $user->getUuid()])) {
                    continue;
                }
                $this->entityManager->persist(UserFromApiFactory::create($user));
            }
            $this->entityManager->flush();

            return new JsonResponse($this->sezializer->serialize(
                ['success' => sprintf('Users created: %s', count($usersResponse->getData()))], 'json', ['groups' => ['get']]
            ), Response::HTTP_OK, [], true);

        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage(), 'exception_class' => get_class($e)], Response::HTTP_BAD_REQUEST, []);
        }
    }

}