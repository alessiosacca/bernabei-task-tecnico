<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\ApiClientException;
use App\Service\ApiUserClient;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

#[Route('/users/{filter}', name: 'get_users', methods: 'GET')]
class GetUsers extends AbstractController
{

    private ApiUserClient $apiUserClient;
    private SerializerInterface $sezializer;
    private EntityManagerInterface $entityManager;

    public function __construct(ApiUserClient $apiUserClient, SerializerInterface $sezializer, EntityManagerInterface $entityManager)
    {
        $this->apiUserClient = $apiUserClient;
        $this->sezializer = $sezializer;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws ApiClientException
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function __invoke($filter = null): JsonResponse
    {
        try {
            $criteria = Criteria::create();
            if ($filter) {
                $criteria
                    ->orWhere(Criteria::expr()->contains('firstname', $filter))
                    ->orWhere(Criteria::expr()->contains('lastname', $filter))
                    ->orWhere(Criteria::expr()->contains('username', $filter))
                    ->orWhere(Criteria::expr()->contains('email', $filter));
            }

            $users = $this->entityManager->getRepository(User::class)->matching($criteria);

            return new JsonResponse($this->sezializer->serialize(
                ['users' => $users], 'json', ['groups' => ['get']]
            ), Response::HTTP_OK, [], true);

        } catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage(), 'exception_class' => get_class($e)], Response::HTTP_BAD_REQUEST, []);
        }
    }

}