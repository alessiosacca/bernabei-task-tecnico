<?php

namespace App\Factory;

use App\Entity\User;

class UserFromApiFactory
{

    /**
     * @param \App\Service\Response\User $userData
     * @return User
     */
    public static function create(\App\Service\Response\User $userData): User
    {
        $user = new User();
        $user->setUuid($userData->getUuid());
        $user->setFirstname($userData->getFirstname());
        $user->setLastname($userData->getLastname());
        $user->setUsername($userData->getUsername());
        $user->setEmail($userData->getEmail());
        $user->setIp($userData->getIp());
        $user->setWebsite($userData->getWebsite());

        return $user;
    }
}