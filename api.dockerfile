FROM php:8.2-fpm
RUN apt-get update
RUN apt-get install -y libzip-dev
RUN docker-php-ext-install pdo pdo_mysql zip

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER=1
COPY ./api/ /var/www/api
WORKDIR /var/www/api
RUN composer install
#RUN php bin/console doctrine:migrations:migrate