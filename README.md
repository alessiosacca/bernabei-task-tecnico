# Bernabei - Task Tecnico

### Task Description

#### Back-end

- consumare una api pubblica qualsiasi (sei libero di scegliere un servizio a piacere, ad esempio come https://any-api.com/ o https://apilist.fun/)
- aggregare i dati ricevuti in qualche modo
- prevedere uno scope per i dati salvati (pubblico/autenticato/entrambi)
- persistere i dati rilevanti
- esporre una nuova api contenuta nell’applicazione per cercare/recuperare i dati salvati

#### Front-end
- effettuare ricerche e visualizzare i dati recuperati (pubblici)
- TODO - effettuare login per autenticarsi
- TODO - effettuare ricerche da loggati visualizzando anche i dati con scope autenticato.


### How to run
```
docker-compose up -d --build
```
```
docker exec -it php_symfony_task php bin/console doctrine:migrations:migrate
```
Navigate to `http://localhost:8081`

Front-end Web Server port is defined in `docker-compose.yml`