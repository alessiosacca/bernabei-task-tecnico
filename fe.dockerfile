FROM node:lts-alpine
RUN npm install -g http-server
WORKDIR /app

COPY ./fe/package*.json ./
RUN npm install

COPY ./fe .

RUN npm run build

EXPOSE 8080
CMD [ "http-server", "build" ]