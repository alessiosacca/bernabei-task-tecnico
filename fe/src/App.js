import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Header from "./features/Header";
import TableUser from "./features/TableUser";


function App() {
    return (
        <div className="App">
            <div className="container py-3">
                <Header/>
                <TableUser/>
            </div>
        </div>
    );
}

export default App;
