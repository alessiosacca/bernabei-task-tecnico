import React, {useEffect, useState} from "react";
import {Alert, Button, InputGroup, Spinner, Stack, Table, Form} from "react-bootstrap";
import axios from "axios";

function TableUser() {

    const [msg, setMsg] = useState({type: '', msg: ''})
    const [isLoading, setIsLoading] = useState(false)
    const [users, setUsers] = useState([])
    const [filter, setFilter] = useState('')

    useEffect(() => {
        getUsers(filter)
    }, [])

    const updateUsers = () => {
        setIsLoading(true)
        axios
            .post('http://localhost:8080/users/1')
            .then(response => {
                if (response?.data?.success) {
                    setMsg({
                        type: 'success',
                        msg: response?.data?.success
                    })
                }
            })
            .catch(error => {
                console.log('error', error)
                setMsg({
                    type: 'danger',
                    msg: error?.response?.data?.detail ? error?.response?.data?.detail : 'Error in Api Users'
                })

            })
            .finally(() => getUsers(filter))
    }


    const getUsers = (filter) => {
        setIsLoading(true)
        axios
            .get(`http://localhost:8080/users/${filter}`)
            .then(response => {
                setUsers(response?.data?.users || [])
            })
            .catch(error => {
                console.log('error', error)
                setMsg({
                    type: 'danger',
                    msg: error?.response?.data?.detail ? error?.response?.data?.detail : 'Error in Api Users'
                })

            })
            .finally(() => setIsLoading(false))
    }

    return (
        <Stack gap={3}>
            {msg.msg !== '' ?
                <Alert key={'apiMessage'} variant={msg.type}>
                    {msg.msg}
                </Alert> : null}
            <div className="d-flex justify-content-between">
                <Button variant={'outline-primary'} onClick={() => updateUsers()}>Add User From Api</Button>
                <InputGroup className="mb-3 w-auto">
                    <Form.Control
                        placeholder="Search"
                        aria-label="Search"
                        aria-describedby="search-button"
                        onChange={(e) => setFilter(e.target.value)}
                    />
                    <Button variant="outline-secondary" id="search-button" onClick={() => getUsers(filter)}>
                        Search
                    </Button>
                </InputGroup>
            </div>
            <div className="d-flex justify-content-center">
                {isLoading ? <Spinner animation="grow" variant="info"/> :
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Ip</th>
                            <th>Website</th>
                        </tr>
                        </thead>
                        <tbody>
                        {users.map((user, key) => (
                            <tr>
                                <td>{user.id}</td>
                                <td>{user.firstname}</td>
                                <td>{user.lastname}</td>
                                <td>{user.username}</td>
                                <td>{user.email}</td>
                                <td>{user.ip}</td>
                                <td>{user.website}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>}
            </div>
        </Stack>

    )
}

export default TableUser