import App from "../App";

function Header() {

    return (
        <div className="d-flex flex-column flex-md-row align-items-center pb-1 mb-4 border-bottom">
            <a className="d-flex align-items-baseline text-dark text-decoration-none">
                <span className="fs-2 text-primary">Bernabei</span>
                <span className="fs-6 ms-2">Task Tecnico</span>
            </a>
        </div>
    )
}

export default Header;